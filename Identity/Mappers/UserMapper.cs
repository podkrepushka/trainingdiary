﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Training.Domain.Entities.Identity;
using Training.Identity.Identities;

namespace Training.Identity.Mappers
{
    class UserMapper
    {
        public UserMapper()
        {
            Mapper.CreateMap<IdentityUser, User>();
            Mapper.CreateMap<User, IdentityUser>();
        }

        public  User GetUser(IdentityUser identityUser)
        {
            return Mapper.Map<IdentityUser, User>(identityUser);
        }

        public  IdentityUser GetIdentityUser(User user)
        {
            return Mapper.Map<User, IdentityUser>(user);
        }
    }
}
