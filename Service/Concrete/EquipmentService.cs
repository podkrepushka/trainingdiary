﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Domain.Abstract.UnitOfWorks;
using Training.Domain.Entities.Application;
using Training.Service.Abstract;

namespace Training.Service.Concrete
{
    public class EquipmentService : IEquipmentService
    {
        private IUnitOfWork UnitOfWork;

        public EquipmentService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public Task<List<Equipment>> GetAllAsync()
        {
            return UnitOfWork.EquipmentRepository.GetAllAsync();
        }

        public Task<Equipment> FindByIdAsync(object id)
        {
            return UnitOfWork.EquipmentRepository.FindByIdAsync(id);
        }

        public Task<int> CreateAsync(Equipment equipment)
        {
            UnitOfWork.EquipmentRepository.Add(equipment);
            return UnitOfWork.SaveChangesAsync();
        }

        public Task<int> UpdateAsync(Equipment equipment)
        {
            UnitOfWork.EquipmentRepository.Update(equipment);
            return UnitOfWork.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(Equipment equipment)
        {
            UnitOfWork.EquipmentRepository.Remove(equipment);
            return UnitOfWork.SaveChangesAsync();
        }

    }
}
