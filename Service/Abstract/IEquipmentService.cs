﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Domain.Abstract.UnitOfWorks;
using Training.Domain.Entities.Application;

namespace Training.Service.Abstract
{
    interface IEquipmentService
    {

        Task<List<Equipment>> GetAllAsync();
        Task<Equipment> FindByIdAsync(object id);
        Task<int> CreateAsync(Equipment equipment);
        Task<int> UpdateAsync(Equipment equipment);
        Task<int> DeleteAsync(Equipment equipment);

    }
}
