﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Domain.Entities.Application
{
    public class Exercise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? EquipmentId { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual ICollection<ComplexExerciese> ComplexExrcises { get; set; }
    }

}
