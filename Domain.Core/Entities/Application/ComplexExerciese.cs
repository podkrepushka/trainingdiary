﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Domain.Entities.Application
{
    public class ComplexExerciese
    {
        public int Id { get; set; }
        public int ExecutionOrder { get; set; }
        public double Weight { get; set; }
        public int ExerciseWorkoutId { get; set; }
        public int? OptionId { get; set; }
        public int ExerciseId { get; set; }
        

        public virtual ExerciseWorkout ExerciseWorkout { get; set; }
        public virtual Exercise Exercise { get; set; }
        public virtual OptionExercises Option { get; set; }
        public virtual ICollection<ApproachExercise> Approachs { get; set; }
    }
}
