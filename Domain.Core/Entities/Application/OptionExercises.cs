﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Domain.Entities.Application
{
    public class OptionExercises
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ComplexExerciese> ComplexExercieses { get; set; }
    }
}
