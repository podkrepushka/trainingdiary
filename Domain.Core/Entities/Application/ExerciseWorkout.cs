﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Domain.Entities.Application
{
    public class ExerciseWorkout
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int Sequence { get; set; }

        public virtual Workout Workout { get; set; }
        public virtual ICollection<ComplexExerciese> ComplexExercises { get; set; }
    }
}
