﻿using System.Collections.Generic;

namespace Training.Domain.Entities.Identity
{
    public class Role
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<User> Users { get; set; }
        #endregion
    }
}
