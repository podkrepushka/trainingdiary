﻿using Training.Domain.Abstract.Repositories;
using Training.Domain.Entities.Application;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Training.Domain.Abstract.UnitOfWorks
{
    public interface IUnitOfWork : IDisposable
    {
        #region Properties
        IExternalLoginRepository ExternalLoginRepository { get; }
        IRoleRepository RoleRepository { get; }
        IUserRepository UserRepository { get; }
        IRepository<Exercise> ExerciseRepository { get; }
        IRepository<OptionExercises> OptionExercisesRepository { get; } 
        IRepository<Equipment> EquipmentRepository { get; }
        IRepository<Workout> WorkoutRepository { get; }
        IRepository<ExerciseWorkout> ExerciseWorkoutRepository { get; }
        IRepository<ComplexExerciese> ComplexExercieseRepository { get; }
        IRepository<ApproachExercise> ApproachExerciseRepository { get; }
        #endregion

        #region Methods
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        #endregion
    }
}
