﻿using Training.Domain.Entities.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace  Training.Domain.Abstract.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByUserName(string username);
        Task<User> FindByUserNameAsync(string username);
        Task<User> FindByUserNameAsync(CancellationToken cancellationToken, string username);
        User FindByUserEmail(string email);
        Task<User> FindByUserEmailAsync(string email);
    }
}
