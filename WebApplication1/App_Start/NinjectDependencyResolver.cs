﻿using Training.Data.EntityFramework.Concrete.UnitOfWorks;
using Training.Domain.Abstract.UnitOfWorks;
using Training.Identity.Identities;
using Training.Identity.Managers;
using Training.Identity.Stores;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Ninject;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin;
using System.Web;



namespace Training.WebUI.App_Start
{
    class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IAppBuilder app)
        {
            kernel = new StandardKernel();
            AddBinding(app);
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBinding(IAppBuilder app)
        {
            //Binding
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            #region Identity Managers and AuthenticationManager
            kernel.Bind<IUserStore<IdentityUser, int>>().To<CustomUserStore>();

            kernel.Bind<UserManager<IdentityUser, int>>()
                  .To<ApplicationUserManager>()
                  .WithConstructorArgument<IDataProtectionProvider>(app.GetDataProtectionProvider());

            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication);
            #endregion

            
        }
    }
}
