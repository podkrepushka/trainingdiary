﻿using Microsoft.Owin;
using Owin;
using System.Web.Mvc;
using Training.WebUI.App_Start;

[assembly: OwinStartupAttribute(typeof(Training.WebUI.Startup))]
namespace Training.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            DependencyResolver.SetResolver(new NinjectDependencyResolver(app));
        }
    }
}
