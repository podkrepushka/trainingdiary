﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Domain.Entities.Application;
using Training.UnitTests.Moq.TestDbAsync;

namespace Training.UnitTests.Moq.DbSets
{
    public static class MoqEquipmentDbSet
    {
        public static Mock<DbSet<Equipment>> Get()
        {
            var Equipments = new List<Equipment>
            {
                new Equipment { Id = 1, Name = "Штанга"},
                new Equipment { Id = 2, Name = "Гантели"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Equipment>>();

            mockSet.As<IDbAsyncEnumerable<Equipment>>()
               .Setup(m => m.GetAsyncEnumerator())
               .Returns(new TestDbAsyncEnumerator<Equipment>(Equipments.GetEnumerator()));

            mockSet.As<IQueryable<Equipment>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<Equipment>(Equipments.Provider));

            mockSet.As<IQueryable<Equipment>>().Setup(m => m.Expression).Returns(Equipments.Expression);
            mockSet.As<IQueryable<Equipment>>().Setup(m => m.ElementType).Returns(Equipments.ElementType);
            mockSet.As<IQueryable<Equipment>>().Setup(m => m.GetEnumerator()).Returns(Equipments.GetEnumerator());

            return mockSet;
        }
    }
}
