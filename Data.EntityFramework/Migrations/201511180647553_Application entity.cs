namespace Training.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Applicationentity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApproachsExercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Repeats = c.Int(),
                        Approach = c.Int(nullable: false),
                        ComplexExerciseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ComplexExercises", t => t.ComplexExerciseId, cascadeDelete: true)
                .Index(t => t.ComplexExerciseId);
            
            CreateTable(
                "dbo.ComplexExercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExecutionOrder = c.Int(),
                        Weight = c.Double(nullable: false),
                        ExerciseWorkoutId = c.Int(nullable: false),
                        OptionId = c.Int(),
                        ExerciseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercises", t => t.ExerciseId, cascadeDelete: true)
                .ForeignKey("dbo.ExercisesWorkouts", t => t.ExerciseWorkoutId, cascadeDelete: true)
                .ForeignKey("dbo.OptionsExercises", t => t.OptionId)
                .Index(t => t.ExerciseWorkoutId)
                .Index(t => t.OptionId)
                .Index(t => t.ExerciseId);
            
            CreateTable(
                "dbo.Exercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        EquipmentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipments", t => t.EquipmentId)
                .Index(t => t.EquipmentId);
            
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExercisesWorkouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkoutId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Workouts", t => t.WorkoutId, cascadeDelete: true)
                .Index(t => t.WorkoutId);
            
            CreateTable(
                "dbo.Workouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.OptionsExercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApproachsExercises", "ComplexExerciseId", "dbo.ComplexExercises");
            DropForeignKey("dbo.ComplexExercises", "OptionId", "dbo.OptionsExercises");
            DropForeignKey("dbo.ComplexExercises", "ExerciseWorkoutId", "dbo.ExercisesWorkouts");
            DropForeignKey("dbo.ExercisesWorkouts", "WorkoutId", "dbo.Workouts");
            DropForeignKey("dbo.Workouts", "UserId", "dbo.Users");
            DropForeignKey("dbo.ComplexExercises", "ExerciseId", "dbo.Exercises");
            DropForeignKey("dbo.Exercises", "EquipmentId", "dbo.Equipments");
            DropIndex("dbo.Workouts", new[] { "UserId" });
            DropIndex("dbo.ExercisesWorkouts", new[] { "WorkoutId" });
            DropIndex("dbo.Exercises", new[] { "EquipmentId" });
            DropIndex("dbo.ComplexExercises", new[] { "ExerciseId" });
            DropIndex("dbo.ComplexExercises", new[] { "OptionId" });
            DropIndex("dbo.ComplexExercises", new[] { "ExerciseWorkoutId" });
            DropIndex("dbo.ApproachsExercises", new[] { "ComplexExerciseId" });
            DropTable("dbo.OptionsExercises");
            DropTable("dbo.Workouts");
            DropTable("dbo.ExercisesWorkouts");
            DropTable("dbo.Equipments");
            DropTable("dbo.Exercises");
            DropTable("dbo.ComplexExercises");
            DropTable("dbo.ApproachsExercises");
        }
    }
}
