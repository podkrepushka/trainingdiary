// <auto-generated />
namespace Training.Data.EntityFramework.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Applicationentity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Applicationentity));
        
        string IMigrationMetadata.Id
        {
            get { return "201511180647553_Application entity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
