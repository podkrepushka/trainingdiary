﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class ExerciseWorkoutConfiguration : EntityTypeConfiguration<ExerciseWorkout>
    {
        public ExerciseWorkoutConfiguration()
        {
            ToTable("ExercisesWorkouts");

            HasKey(x => x.Id)
              .Property(x => x.Id)
              .HasColumnName("Id")
              .HasColumnType("int")
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
              .IsRequired();

            Property(x => x.Sequence)
                .HasColumnName("Sequence")
                .HasColumnType("int")
                .IsRequired();

            HasRequired(x => x.Workout)
                .WithMany(x => x.ExercisesWorkout)
                .HasForeignKey(x => x.WorkoutId);

            HasMany(x => x.ComplexExercises)
                .WithRequired(x => x.ExerciseWorkout)
                .HasForeignKey(x => x.ExerciseWorkoutId);
        }
    }
}
