﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class WorkoutConfiguration : EntityTypeConfiguration<Workout>
    {
        public WorkoutConfiguration()
        {
            ToTable("Workouts");

            HasKey(x => x.Id)
               .Property(x => x.Id)
               .HasColumnName("Id")
               .HasColumnType("int")
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
               .IsRequired();

            Property(x => x.Date)
                .HasColumnName("Date")
                .HasColumnType("date")
                .IsRequired(); 

            HasRequired(x => x.User)
                .WithMany(x => x.Workouts)
                .HasForeignKey(x => x.UserId);

            HasMany(x => x.ExercisesWorkout)
                .WithRequired(x => x.Workout)
                .HasForeignKey(x => x.WorkoutId);
        }
    }
}
