﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class ApproachConfiguration : EntityTypeConfiguration<ApproachExercise>
    {
        public ApproachConfiguration()
        {
            ToTable("ApproachsExercises");

            HasKey(x => x.Id)
              .Property(x => x.Id)
              .HasColumnName("Id")
              .HasColumnType("int")
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
              .IsRequired();

            Property(x => x.Repeats)
                .HasColumnName("Repeats")
                .HasColumnType("int")
                .IsOptional();

            HasRequired(x => x.ComplexExercise)
                .WithMany(x => x.Approachs)
                .HasForeignKey(x => x.ComplexExerciseId);

        }
    }
}
