﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class ExerciseConfiguration : EntityTypeConfiguration<Exercise>
    {
        internal ExerciseConfiguration()
        {
            ToTable("Exercises");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("Id")
                .HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("nvarchar")
                .HasMaxLength(256)
                .IsRequired();

            HasOptional(x => x.Equipment)
                .WithMany(x => x.Exercise)
                .HasForeignKey(x => x.EquipmentId);

            HasMany(x => x.ComplexExrcises)
                .WithRequired(x => x.Exercise)
                .HasForeignKey(x => x.ExerciseId);
        }
    }
}
