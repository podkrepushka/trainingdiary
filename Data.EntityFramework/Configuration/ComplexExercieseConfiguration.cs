﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class ComplexExercieseConfiguration : EntityTypeConfiguration<ComplexExerciese>
    {
        internal ComplexExercieseConfiguration()
        {
            ToTable("ComplexExercises");

            HasKey(x => x.Id)
               .Property(x => x.Id)
               .HasColumnName("Id")
               .HasColumnType("int")
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
               .IsRequired();

            Property(x => x.ExecutionOrder)
                .HasColumnName("ExecutionOrder")
                .HasColumnType("int")
                .IsOptional(); //

            Property(x => x.Weight)
                .HasColumnName("Weight")
                .HasColumnType("float")
                .IsRequired();

            HasRequired(x => x.Exercise)
                .WithMany(x => x.ComplexExrcises)
                .HasForeignKey(x => x.ExerciseId);

            HasOptional(x => x.Option)
                .WithMany(x => x.ComplexExercieses)
                .HasForeignKey(x => x.OptionId);

            HasRequired(x => x.ExerciseWorkout)
                .WithMany(x => x.ComplexExercises)
                .HasForeignKey(x => x.ExerciseWorkoutId);

            HasMany(x => x.Approachs)
                .WithRequired(x => x.ComplexExercise)
                .HasForeignKey(x => x.ComplexExerciseId);
        }
    }
}
