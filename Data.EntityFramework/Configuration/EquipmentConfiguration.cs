﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.Configuration
{
    public class EquipmentConfiguration : EntityTypeConfiguration<Equipment>
    {
        internal EquipmentConfiguration()
        {
            ToTable("Equipments");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("Id")
                .HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("nvarchar")
                .HasMaxLength(256)
                .IsRequired();

            HasMany(x => x.Exercise)
                .WithOptional(x => x.Equipment)
                .HasForeignKey(x => x.EquipmentId);
        }
    }
}
