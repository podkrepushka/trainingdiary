﻿using Training.Data.EntityFramework.Configuration;
using Training.Domain.Entities.Identity;
using System.Data.Entity;
using Training.Domain.Entities.Application;

namespace Training.Data.EntityFramework.DBContext
{
    /*internal*/ public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(string nameOrConnectionString = "TrainingConnection")
            : base(nameOrConnectionString)
        {
        }


        internal IDbSet<User> Users { get; set; }
        internal IDbSet<Role> Roles { get; set; }
        internal IDbSet<ExternalLogin> ExternalLogins { get; set; }

        /*internal*/ public virtual DbSet<Equipment> Equipments { get; set; }
        /*internal*/ public IDbSet<Exercise> Exercises { get; set; }
        internal IDbSet<OptionExercises> OptionsExercises { get; set; }
        internal IDbSet<Workout> Workouts { get; set; }
        internal IDbSet<ExerciseWorkout>  ExercisesWorkouts { get; set; }
        internal IDbSet<ComplexExerciese> ComplexExercieses { get; set; }
        internal IDbSet<ApproachExercise> ApproachsExercises { get; set; }

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new ExternalLoginConfiguration());
            modelBuilder.Configurations.Add(new ClaimConfiguration());

            modelBuilder.Configurations.Add(new EquipmentConfiguration());
            modelBuilder.Configurations.Add(new ExerciseConfiguration());
            modelBuilder.Configurations.Add(new OptionExercisesConfiguration());
            modelBuilder.Configurations.Add(new WorkoutConfiguration());
            modelBuilder.Configurations.Add(new ExerciseWorkoutConfiguration());
            modelBuilder.Configurations.Add(new ComplexExercieseConfiguration());
            modelBuilder.Configurations.Add(new ApproachConfiguration());


        }
    }
}