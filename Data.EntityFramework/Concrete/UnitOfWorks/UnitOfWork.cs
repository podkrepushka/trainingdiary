﻿using Training.Data.EntityFramework.Concrete.Repositories;
using Training.Data.EntityFramework.DBContext;
using Training.Domain.Abstract.Repositories;
using Training.Domain.Abstract.UnitOfWorks;
using System.Threading.Tasks;
using Training.Domain.Entities.Application;
namespace Training.Data.EntityFramework.Concrete.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
        private readonly ApplicationDbContext Context;
        private IExternalLoginRepository externalLoginRepository;
        private IRoleRepository roleRepository;
        private IUserRepository userRepository;
        private IRepository<Exercise> exerciseRepository;
        private IRepository<OptionExercises> optionExercisesRepository;
        private IRepository<Equipment> equipmentRepository;
        private IRepository<Workout> workoutRepository;
        private IRepository<ExerciseWorkout> exerciseWorkoutRepository;
        private IRepository<ComplexExerciese> complexExercieseRepository;
        private IRepository<ApproachExercise> approachExerciseRepository;
        #endregion

        #region Constructors
        public UnitOfWork()//(string nameOrConnectionString)
        {
            Context = new ApplicationDbContext();// (nameOrConnectionString);
        }

        //public UnitOfWork(ApplicationDbContext context)
        //{
        //    Context = context;
        //}
        #endregion

        #region IUnitOfWork Members
        public IExternalLoginRepository ExternalLoginRepository
        {
            get { return externalLoginRepository ?? (externalLoginRepository = new ExternalLoginRepository(Context)); }
        }

        public IRoleRepository RoleRepository
        {
            get { return roleRepository ?? (roleRepository = new RoleRepository(Context)); }
        }

        public IUserRepository UserRepository
        {
            get { return userRepository ?? (userRepository = new UserRepository(Context)); }
        }

        public IRepository<Exercise> ExerciseRepository 
        {
            get { return exerciseRepository ?? (exerciseRepository = new Repository<Exercise>(Context)); }
        }

        public IRepository<OptionExercises> OptionExercisesRepository 
        {
            get { return optionExercisesRepository ?? ( optionExercisesRepository = new Repository<OptionExercises>(Context)); }
        }
       
        public IRepository<Equipment> EquipmentRepository
        {
            get { return equipmentRepository ?? ( equipmentRepository = new Repository<Equipment>(Context)); }
        }
        
        public IRepository<Workout> WorkoutRepository 
        {
            get { return workoutRepository ?? (workoutRepository = new Repository<Workout>(Context)); }
        }
        
        public IRepository<ExerciseWorkout> ExerciseWorkoutRepository 
        {
            get { return exerciseWorkoutRepository ?? (exerciseWorkoutRepository = new Repository<ExerciseWorkout>(Context)); }
        }
            
        public IRepository<ComplexExerciese> ComplexExercieseRepository 
        {
            get { return complexExercieseRepository ?? ( complexExercieseRepository = new Repository<ComplexExerciese>(Context)); }
        }
            
        public IRepository<ApproachExercise> ApproachExerciseRepository 
        {
            get { return approachExerciseRepository ?? ( approachExerciseRepository = new Repository<ApproachExercise>(Context)); }
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return Context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return Context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            externalLoginRepository = null;
            roleRepository = null;
            userRepository = null;
            Context.Dispose();
        }
        #endregion
    }
}