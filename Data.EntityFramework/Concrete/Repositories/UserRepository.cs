﻿using Training.Data.EntityFramework.DBContext;
using Training.Domain.Abstract.Repositories;
using Training.Domain.Entities.Identity;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Training.Data.EntityFramework.Concrete.Repositories
{
    internal class UserRepository : Repository<User>, IUserRepository
    {
        internal UserRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public User FindByUserName(string username)
        {
            return Set.FirstOrDefault(x => x.UserName == username);
        }

        public Task<User> FindByUserNameAsync(string username)
        {
            return Set.FirstOrDefaultAsync(x => x.UserName == username);
        }

        public Task<User> FindByUserNameAsync(System.Threading.CancellationToken cancellationToken, string username)
        {
            return Set.FirstOrDefaultAsync(x => x.UserName == username, cancellationToken);
        }

        public User FindByUserEmail(string email)
        {
            return Set.FirstOrDefault(x => x.Email.ToUpper() == email.ToUpper());
        }

        public Task<User> FindByUserEmailAsync(string email)
        {
            return Set.FirstOrDefaultAsync(x => x.Email.ToUpper() == email.ToUpper());
        }
    }
}